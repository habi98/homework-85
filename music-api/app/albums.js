const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');
const permit = require('../middleware/permit');

const Album = require('../models/Album');
const Track = require('../models/Track');


const router = express.Router();


    const storage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, config.uploadPath)
        },
        filename: (req, file, cb) => {
            cb(null, nanoid() + path.extname(file.originalname));
        }
    });


    const upload = multer({storage});

        router.get('/', tryAuth, (req, res) => {

            let query = {published: true, delete: false};

            if (req.query.artist && req.user && req.user.role === 'admin') {
                query = {artist: req.query.artist, delete: false}
            } else if(req.query.artist) {
                query = {artist: req.query.artist, published: true, delete: false}
            }

            Album.find(query).sort({year: 1}).populate('artist')
                .then(artists => res.send(artists))
                .catch(()=> res.sendStatus(500))

        });

        router.get('/:id', tryAuth, (req, res) => {

            let criteria = {_id: req.params.id, published: true};

            if (req.user && req.user.role === 'admin') {
                criteria =  {_id: req.params.id}
            }

            Album.findOne(criteria).populate('artist')
                .then(album => res.send(album))
                .catch(() => res.sendStatus(500))
        });

     router.post('/', auth, upload.single('image'), (req, res) => {
        const albumData = req.body;

        if (req.file) {
            albumData.image = req.file.filename
        }

        const album = new Album(albumData) ;

        album.user = req.user._id;

        album.save()
            .then(result => res.send(result))
            .catch(error => res.sendStatus(400).send(error))
    });

router.post('/:id/toggle_published', [auth, permit('admin')], async (req, res) => {

    const album = await Album.findById(req.params.id);

    if (!album) {
        return res.sendStatus(404);
    }

    album.published = !album.published;

    await album.save();

    res.send(album);
});


router.delete('/delete/:id', [auth, permit('admin')], async (req, res) => {

    try {

        const album = await Album.findById(req.params.id);


        await Album.updateOne({_id: album._id}, {$set: {delete: true}});

        if (album.length <= 0) {
            res.sendStatus(200)
        }

        await Track.updateMany({album: album._id}, {$set: {delete: true}});


        res.status(200).send(album)
    } catch (e) {
        res.send(e)
    }

});


module.exports = router;