import {FETCH_TRACKS_SUCCESS} from "../actions/tracksAction";

const initialState = {
    tracks: null
};

const tracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACKS_SUCCESS:
            return {...state, tracks: action.tracks};
        default:
            return state
    }
};

export default tracksReducer;