import React from 'react';
import CardImg from "reactstrap/es/CardImg";

const ProductThumbnail = (props) => {
    let image;

    if (props.image) {
        image = 'http://localhost:8000/uploads/' + props.image;
    }

    return <CardImg top width="100%" src={image} alt="Card image cap" />
};

export default ProductThumbnail;
