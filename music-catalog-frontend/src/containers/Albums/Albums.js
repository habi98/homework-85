import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Card, CardBody, CardColumns, CardText} from "reactstrap";
import ProductThumbnail from "../../components/ArtistsThumbnail/ArtistsThumbnail";
import {Link} from "react-router-dom";
import {deleteAlbum, fetchAlbums, publishAlbum} from "../../store/actions/albumsAction";

class Albums extends Component {

    componentDidMount() {
        this.props.fetchAlbums(this.props.match.params.id)
    }

    render() {
        return (
            <Fragment>
            {this.props.albums && this.props.albums.length > 0 ?

                <Fragment>
                <CardColumns>
                    {this.props.albums.map(album => (
                                <Card key={album._id}>
                                    <ProductThumbnail image={album.image}/>
                                    <CardBody>
                                        <Link to={'/albums/' + album._id}>
                                            {album.name}
                                        </Link>
                                        <CardText>{album.year} г.</CardText>
                                        {this.props.user && this.props.user.role === 'admin' && (
                                            <Fragment>
                                                {album.published === false ? (
                                                    <Fragment>
                                                        <p>Неопубликовано</p>
                                                        <CardText>
                                                            <Button onClick={() => this.props.publishAlbum(album._id, album.artist._id)} color='primary'>
                                                                Опубликовать
                                                            </Button>
                                                        </CardText>
                                                    </Fragment>
                                                ): null}
                                                <Button onClick={() => this.props.deleteAlbum(album._id, album.artist._id)} color='danger'>
                                                    Удалить
                                                </Button>
                                            </Fragment>

                                        )}
                                    </CardBody>
                                </Card>
                    ))}
                </CardColumns>
            </Fragment> : <h2>Album not Published</h2>}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
   albums: state.albums.albums,
   user: state.users.user
});


const mapDispatchToProps = dispatch => ({
    fetchAlbums: id => dispatch(fetchAlbums(id)),
    publishAlbum: (albumId, artistId) => dispatch(publishAlbum(albumId, artistId)),
    deleteAlbum: (albumId, artistId) => dispatch(deleteAlbum(albumId, artistId)),

});
export default connect(mapStateToProps, mapDispatchToProps)(Albums);