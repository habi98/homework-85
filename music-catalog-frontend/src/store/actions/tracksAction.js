import axios from "../../axios-music";
import {push} from 'connected-react-router';
export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACK_SUCCESS';

export const fetchTracksSuccess = tracks => ({type: FETCH_TRACKS_SUCCESS, tracks});

export const fetchTracks = (id) => {
    return dispatch => {
        axios.get('/tracks?album=' + id).then(response => {
            dispatch(fetchTracksSuccess(response.data))
        })
    }
};

export const CREATE_TRACK_SUCCESS = 'CREATE_TRACK_SUCCESS';

export const createTrackSuccess = () =>  ({type: CREATE_TRACK_SUCCESS});

export const createTrack = data => {
    return dispatch => {
        axios.post('/tracks', data).then(
            () => dispatch(createTrackSuccess()),
            dispatch(push('/'))
        )
    }
};


export const PUBLISH_TRACKS_SUCCESS = 'PUBLISH_TRACKS_SUCCESS';
export const publishTracksSuccess = () => ({type: PUBLISH_TRACKS_SUCCESS});


export const publishTracks = (id, albumId) => {
    return dispatch  => {
        axios.post(`/tracks/${id}/toggle_published`).then(
            () => {
                dispatch(publishTracksSuccess());
                dispatch(fetchTracks(albumId))
            }
        )
    }
};