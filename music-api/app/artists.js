
const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');

const Artist = require('../models/Artist');
const Album = require('../models/Album');
const Track = require('../models/Track');

const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');
const permit = require('../middleware/permit');


const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});


    router.get('/',  tryAuth, (req, res) => {

        let published = {published: true, delete: false};

        if (req.user && req.user.role === 'admin') {
            published = {delete: false}
        }

        Artist.find(published)
            .then(artists => res.send(artists))
            .catch(()=> res.sendStatus(500));
    });

    router.post('/', auth ,upload.single('image'), (req, res) => {
       const artistData = req.body;

       if (req.file) {
           artistData.image = req.file.filename
       }

       const artist = new Artist(artistData);

       artist.user = req.user._id;

       artist.save()
           .then(result => res.send(result))
           .catch(error => res.sendStatus(400).send(error))
    });

    router.post('/:id/toggle_published', [auth, permit('admin')], async (req, res) => {

        const artist = await Artist.findById(req.params.id);

        if (!artist) {
            return res.sendStatus(404);
        }

        artist.published = !artist.published;

        await artist.save();

        res.send(artist);
    });


    router.delete('/delete/:id', [auth, permit('admin')], async (req, res) => {

        try {
            const artist = await Artist.findById(req.params.id);

            await Artist.updateOne({_id: artist._id}, {$set: {delete: true}});


            if (!artist) {
                res.sendStatus(400)
            }

           const album = await Album.find({artist: artist._id});


            await Album.updateMany({artist: artist._id}, {$set: {delete: true}});

            if (album.length <= 0) {
                res.sendStatus(200)
            }

            await  Promise.all(album.map(oneAlbum => Track.updateMany({album: oneAlbum._id}, {$set: {delete: true}})));


            res.status(200).send(artist)
        } catch (e) {
            res.send(e)
        }

    });


module.exports = router;

