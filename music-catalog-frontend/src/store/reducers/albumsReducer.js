import {FETCH_ALBUMS_SUCCESS, FETCH_ID_ALBUMS_SUCCESS} from "../actions/albumsAction";

const initialState = {
    albums: null,
    album: null
};

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUMS_SUCCESS:
            return {...state, albums: action.albums};
        case FETCH_ID_ALBUMS_SUCCESS:
            return {
                ...state, album: action.album
            };
        default:
            return state
    }
};

export default albumsReducer;