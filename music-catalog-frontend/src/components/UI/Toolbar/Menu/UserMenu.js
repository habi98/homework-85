import React, {Fragment} from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavLink, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";


const UserMenu = ({user, logout}) => {
    return (
        <Fragment>
            <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret className="d-inline-block">
                    {user.displayName}
                </DropdownToggle>
                <img src={user.avatarImage} style={{display: 'inline-block', height: '40px', borderRadius: '50%', width: '40px'}} alt="avatar"/>

                <DropdownMenu right>
                <NavLink className="p-0" tag={RouterNavLink} to="/add_artist">
                    <DropdownItem>
                        Add New Artist
                    </DropdownItem>
                </NavLink>
                <NavLink className="p-0"  tag={RouterNavLink} to="/add_new_album">
                    <DropdownItem>
                        Add New Album
                    </DropdownItem>
                </NavLink>
                <NavLink className="p-0"  tag={RouterNavLink} to="/add_track">
                    <DropdownItem>
                        Add New Track
                    </DropdownItem>
                </NavLink>
                <NavLink className="p-0"  tag={RouterNavLink} to="/track_history">
                    <DropdownItem>
                        Track History
                    </DropdownItem>
                </NavLink>
                <DropdownItem divider />
                <DropdownItem onClick={logout}>
                    Logout
                </DropdownItem>
            </DropdownMenu>
        </UncontrolledDropdown>


        </Fragment>


    );
};

export default UserMenu;