import React, {Component} from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props'
import {NotificationManager} from 'react-notifications'
import {Button} from "reactstrap";
import {facebookLogin} from "../../store/actions/usersActions";
import {connect} from "react-redux";

class FacebookLogin extends Component {

    facebookLogin = data => {
        console.log(data);
        if (data.error) {
            NotificationManager.error('Something went wrong')
        } else if(!data.name){
            NotificationManager.warning('You pressed cancel');
        } else {
            this.props.facebookLogin(data)
        }
    };
    render() {
        return (
            <FacebookLoginButton
                appId="324867168194894"
                callback={this.facebookLogin}
                fields="name, email, picture"
                render={renderProps => (
                    <Button color="primary" onClick={renderProps.onClick}>Login with Facebook</Button>
                )}
            />
        );
    }
}

const mapDispatchToProps = dispatch => ({
    facebookLogin: userData => dispatch(facebookLogin(userData))
});

export default connect(null, mapDispatchToProps)(FacebookLogin);