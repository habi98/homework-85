import axios from '../../axios-music'
import {push} from 'connected-react-router';
export const FETCH_TRACK_HISTORY_SUCCESS = 'FETCH_TRACK_HISTORY_SUCCESS';
export const TRACK_HISTORY_SUCCESS = 'TRACK_HISTORY_SUCCESS';

export const fetchTrackHistorySuccess = (trackHistory) => ({type: FETCH_TRACK_HISTORY_SUCCESS, trackHistory})
export const trackHistorySuccess = (trackHistory) => ({type: TRACK_HISTORY_SUCCESS, trackHistory});



export const fetchTrackHistory = () => {
     return (dispatch, getState) => {
         const user = getState().users.user;
         if (!user) {
             dispatch(push('/login'))
         } else {
             axios.get('/track_history', {headers: {'Authorization': user.token}}).then(
                 response => {
                     dispatch(fetchTrackHistorySuccess(response.data))
                 }
             )
         }

     }
};


export const trackHistorySend = trackId => {
    return (dispatch, getState) => {
        if (!getState().users.user) return null;

        const token = getState().users.user.token;
        return axios.post('/track_history', trackId, {headers: {'Authorization':  token}}).then(response => {
                dispatch(trackHistorySuccess())
            })
    }
};