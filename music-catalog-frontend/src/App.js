import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import Container from "reactstrap/es/Container";
import {withRouter} from "react-router-dom";
import Routes from "./Routes";
import {logoutUser} from "./store/actions/usersActions";
import {NotificationContainer} from "react-notifications";


class App extends Component {
  state = {
    isOpen: false
  };

    toggle = () => {
     this.setState({
         isOpen: !this.state.isOpen
     })
  };
  render() {
    return (
        <Fragment>
            <Toolbar
                open={this.state.isOpen}
                toggle={this.toggle}
                user={this.props.user}
                logout={this.props.logoutUser}
            />
            <NotificationContainer/>

            <Container>
            <Routes user={this.props.user}/>
         </Container>
        </Fragment>

    );
  }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps  = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
