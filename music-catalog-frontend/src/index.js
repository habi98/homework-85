import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import axios from './axios-music'

import {createBrowserHistory} from "history";
import {connectRouter, routerMiddleware, ConnectedRouter} from "connected-react-router";



import artistsReducer from './store/reducers/artistsReducer'
import albumsReducer from './store/reducers/albumsReducer'
import tracksReducer from './store/reducers/tracksReducer'
import usersReducer from './store/reducers/usersReducer'
import trackHistoryReducer from './store/reducers/trackHistoryReducer'
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-notifications/lib/notifications.css';



const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    albums: albumsReducer,
    artists: artistsReducer,
    tracks: tracksReducer,
    users: usersReducer,
    trackHistory: trackHistoryReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunkMiddleware,
    routerMiddleware(history))));

axios.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().users.user.token
    } catch (e) {

    }

    return config
});

const app = (
    <Provider store={store}>
     <ConnectedRouter history={history}>
            <App/>
     </ConnectedRouter>
    </Provider>

);


ReactDOM.render(app , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
