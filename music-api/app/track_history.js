const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');
const router = express.Router();


router.get('/', async (req, res) => {

    const token = req.get('Authorization');

    if (!token) {
        return res.status(401).send({error: 'Token not provided'})
    }

    const user = await User.findOne({token});

    if(!user) {
        return res.status(401).send({error: 'Token incorrect'})
    }

    const populate = {path: 'track', populate: {path: 'album', populate: {path: 'artist'}}};

    TrackHistory.find({user: user._id}).sort({datetime: 1}).populate(populate)
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500))
});

router.post('/', async (req, res)=> {

    const token = req.get('Authorization');

    const user = await User.findOne({token});

    if (!user) {
        return res.status(401).send({error: 'User not found'})
    }

    const track = req.body.track;

    const existing = await TrackHistory.findOne({track});


    if (existing) {
        return res.send(existing)
    }

    const trackHistory =  new TrackHistory(req.body);

    trackHistory.user = user._id;

    trackHistory.datetime = new Date().toISOString();

    trackHistory.save()
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500));
});


module.exports = router;