const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl: 'mongodb://localhost/music',
    mongoOptions: {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
    facebook: {
        appId: '324867168194894',
        appSecret: '7d8b94c64302156e26264d2b39193471'
    }
};