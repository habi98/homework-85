import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {connect} from "react-redux";
import {fetchArtists} from "../../store/actions/artistsAction";
import {createAlbums} from "../../store/actions/albumsAction";

class FormAddNewAlbum extends Component {

    state = {
        name: '',
        artist: '',
        year: '',
        image: ''
    };

    componentDidMount() {
        console.log(this.props);
        this.props.fetchArtists()
    }

    inputChangeHandler = event => {
        this.setState({
           [event.target.name]: event.target.value
        })
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    submitHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).map(key => {
            return formData.append(key, this.state[key])
        });


        this.props.createAlbums(formData)
    };

    render() {
        return (
          <Form onSubmit={this.submitHandler}>
              <h2>Add Album</h2>
            <FormElement
                propertyName='artist'
                title='Artists'
                type='select'
                value={this.state.artist}
                onChange={this.inputChangeHandler}
            >
                <option value=''>Please select a Artist</option>
                {this.props.artists &&  this.props.artists.map(artist => (
                    <option key={artist._id} value={artist._id}>{artist.name}</option>
                ))}
            </FormElement>
              <FormElement
                  propertyName='name'
                  title='Name'
                  type='text'
                  value={this.state.name}
                  onChange={this.inputChangeHandler}
              />
              <FormElement
                  propertyName='year'
                  title='Date'
                  type='text'
                  value={this.state.year}
                  onChange={this.inputChangeHandler}
              />

              <FormElement
                  propertyName='image'
                  title='Image'
                  type='file'
                  onChange={this.fileChangeHandler}
              />

              <FormGroup row>
                  <Col sm={{offset: 2, size: 10}}>
                      <Button type="submit" color="success">Add artist</Button>
                  </Col>
              </FormGroup>

          </Form>
        );
    }
}


const mapStateToProps = state => ({
    artists: state.artists.artists
});

const mapDispatchToProps = dispatch => ({
    fetchArtists: () => dispatch(fetchArtists),
    createAlbums: albumData => dispatch(createAlbums(albumData))
});


export default connect(mapStateToProps, mapDispatchToProps)(FormAddNewAlbum);