const express = require('express');
const Track = require('../models/Track');
const router = express.Router();
const tryAuth = require('../middleware/tryAuth');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

router.get('/', tryAuth ,(req, res) => {

    let query ;

    if (req.query.album && req.user && req.user.role === 'admin') {
        query = {album: req.query.album, delete: false}
    } else {
        query = {album: req.query.album, published: true, delete: false};
    }

    Track.find(query).sort({trackNumber: 1}).populate('album')
        .then(track => res.send(track))
        .catch(() => res.sendStatus(500))
});

router.post('/', auth, async (req, res) => {
    try {
        const tracks = await Track.find({album: req.body.album});

        const track = new Track(req.body);

        track.user = req.user._id;

        track.trackNumber = tracks.length + 1;

        await track.save();

        res.send(track)
    } catch (e) {
        res.sendStatus(500)
    }

});


router.post('/:id/toggle_published', [auth, permit('admin')], async (req, res) => {
    try {
       await Track.updateOne({_id: req.params.id}, {$set: {published: true}});
        res.status(200).send({message: 'ok'})
    } catch (e) {
        res.sendStatus(500)
    }
});


module.exports = router;


