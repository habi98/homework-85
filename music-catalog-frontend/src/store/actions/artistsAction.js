import axios from "../../axios-music";

export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const fetchArtistsSuccess = artists => ({type: FETCH_ARTISTS_SUCCESS, artists});



export const fetchArtists = () => {
    return dispatch => {
        axios.get('/artists').then(
            response => {
                dispatch(fetchArtistsSuccess(response.data))
            }
        )
    }
};

export const CREATE_ARTISTS_SUCCESS = 'CREATE_ARTISTS_SUCCESS';
export const createArtistsSuccess = () => ({type: CREATE_ARTISTS_SUCCESS});

export const createArtist = (artistData) => {
    return dispatch => {
       return axios.post('/artists', artistData).then(
            () =>  dispatch(createArtistsSuccess())
        )
    }
};


export const PUBLISH_ARTISTS_SUCCESS = 'PUBLISH_ARTISTS_SUCCESS';
export const publishArtistSuccess = () => ({type: PUBLISH_ARTISTS_SUCCESS});


export const publishArtist = (id) => {
    return dispatch  => {
        axios.post(`/artists/${id}/toggle_published`).then(
            () => {
                dispatch(publishArtistSuccess());
                dispatch(fetchArtists())
            }
        )
    }
};


export const DELETE_ARTISTS_SUCCESS = 'DELETE_ARTISTS_SUCCESS';

export const deleteArtistsSuccess = () => ({type: DELETE_ARTISTS_SUCCESS});

export const deleteArtist = (artistId) => {
    return dispatch => {
        axios.delete('/artists/delete/' + artistId).then(
            () => {dispatch(deleteArtistsSuccess());
            dispatch(fetchArtists())}
        )
    }
};
