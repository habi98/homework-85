import React, {Fragment} from 'react';
import {Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import UserMenu from "./Menu/UserMenu";
import AnonymousMenu from "./Menu/AnonymousMenu";

const Toolbar = (props) => {
    return (
        <Fragment>
            <Navbar className="navbar-dark bg-primary"  expand="md">
                <NavbarBrand tag={RouterNavLink} to="/">Music</NavbarBrand>
                <NavbarToggler onClick={props.toggle}/>
                <Collapse isOpen={props.open} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/">Artists</NavLink>
                        </NavItem>

                        {props.user ? <UserMenu user={props.user} logout={props.logout}/> : <AnonymousMenu/> }

                    </Nav>
                </Collapse>
            </Navbar>
        </Fragment>

    );
};

export default Toolbar;