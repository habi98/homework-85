import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Card, CardBody, CardText, CardTitle, ListGroup, ListGroupItem} from "reactstrap";
import {trackHistorySend} from "../../store/actions/trackHistoryActions";
import {fetchTracks, publishTracks} from "../../store/actions/tracksAction";
import {fetchIdAlbums} from "../../store/actions/albumsAction";
class Tracks extends Component {
    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchTracks(id);
        this.props.fetchIdAlbums(id)
    }


    render() {
        return (
           <Fragment>
               {this.props.album ? (
                   <Card>
                       <CardBody>
                           {this.props.album && this.props.album.artist && (
                               <Fragment>
                                   <img className="float-left pr-3" style={{width: '300px', height: '300px'}} src={'http://localhost:8000/uploads/' + this.props.album.artist.image} alt="artist"/>
                                   <CardBody>
                                       <CardTitle>{this.props.album.artist.name}</CardTitle>
                                       <CardText>{this.props.album.artist.text}</CardText>
                                       <CardBody>
                                           <img className="float-left pr-3" style={{width: '70px', height: '50px'}} src={'http://localhost:8000/uploads/' + this.props.album.image} alt="album"/>
                                           <CardTitle className="m-0">Album: {this.props.album.name}</CardTitle>
                                           <p>Date: {this.props.album.year} г.</p>
                                       </CardBody>
                                   </CardBody>
                               </Fragment>

                           )}

                       </CardBody>
                   </Card>
               ): null}
               {this.props.tracks && this.props.tracks.length > 0 ? this.props.tracks.map(track => (
                   <ListGroup onClick={() => this.props.trackHistorySend({track: track._id})} key={track._id}>
                       <ListGroupItem
                         style={{marginTop: '10px'}}>
                           <span>{track.trackNumber} </span>
                            {track.name}
                           <Fragment>
                               <span className="float-right">
                                   {track.duration}
                                   {this.props.user && this.props.user.role  === 'admin' && (
                                       <Fragment>
                                           {track.published === false ? (
                                               <Fragment>
                                                   <span> Необубликован </span>
                                                   <Button onClick={() => this.props.publishTracks(track._id, track.album._id)} color="primary">Обубликовать</Button>
                                               </Fragment>
                                           ): null}
                                           <Button style={{marginLeft: '10px'}} color="danger">Удалить</Button>
                                       </Fragment>
                                   )}
                               </span>
                           </Fragment>

                       </ListGroupItem>
                   </ListGroup>
               )): <h2 className="pt-3">Tracks not Published</h2>}
           </Fragment>


        );
    }
}

const mapStateToProps = state => ({
    tracks: state.tracks.tracks,
    album: state.albums.album,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchTracks: id => dispatch(fetchTracks(id)),
    fetchIdAlbums: id => dispatch(fetchIdAlbums(id)),
    trackHistorySend: (trackId) => dispatch(trackHistorySend(trackId)),
    publishTracks: (id, albumId) => dispatch(publishTracks(id, albumId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);