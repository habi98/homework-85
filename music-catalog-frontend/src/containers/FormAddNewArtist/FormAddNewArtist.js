import React, {Component} from 'react';
import FormElement from "../../components/UI/Form/FormElement";
import {Button, Col, Form, FormGroup} from "reactstrap";
import {createArtist} from "../../store/actions/artistsAction";
import {connect} from "react-redux";

class FormAddNewArtist extends Component {
    state = {
        name: '',
        image: '',
        text: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };


    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })

    };

    submitHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.createArtist(formData).then(this.props.history.push('/'))
    };

    render() {
        return (
            <Form onSubmit={this.submitHandler}>
                <h2>Add Artist</h2>
                <FormElement
                    propertyName='name'
                    title='Name'
                    type="text"
                    value={this.state.name}
                    onChange={this.inputChangeHandler}
                />

                <FormElement
                    propertyName='text'
                    title='Text'
                    type="textarea"
                    value={this.state.text}
                    onChange={this.inputChangeHandler}
                />

                <FormElement
                    propertyName='image'
                    title='Image'
                    type="file"
                    onChange={this.fileChangeHandler}
                />

                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="primary">Add artist</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createArtist: artistData => dispatch(createArtist(artistData))
});

export default connect(null, mapDispatchToProps)(FormAddNewArtist);