import React from 'react';
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import FormAddNewArtist from "./containers/FormAddNewArtist/FormAddNewArtist";
import FormAddNewAlbum from "./containers/FormAddNewAlbum/FormAddNewAlbum";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import Artists from "./containers/Artists/Artists";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import {Redirect, Route, Switch} from "react-router-dom";
import AddNewTrack from "./containers/AddNewTrack/AddNewTrack";

const ProtectedRoute = ({isAllowed, ...props}) => (
    isAllowed ? <Route {...props}/> : <Redirect to="/login"/>
);


const Routes = ({user}) => {
    let validate = (user && (user.role === 'user' || user.role === 'admin'));
    return (
        <Switch>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <ProtectedRoute isAllowed={validate} path="/add_artist" exact component={FormAddNewArtist}/>
            <ProtectedRoute isAllowed={validate} path="/add_new_album" exact component={FormAddNewAlbum}/>
            <ProtectedRoute isAllowed={validate} path="/add_track" exact component={AddNewTrack}/>

            <Route path="/track_history" exact component={TrackHistory}/>
            <Route path="/" exact component={Artists}/>
            <Route path="/artists/:id" exact component={Albums}/>
            <Route path="/albums/:id" exact component={Tracks}/>
        </Switch>
    );
};

export default Routes;