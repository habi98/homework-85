import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Card, CardBody, CardColumns, CardText, CardTitle} from "reactstrap";
import ProductThumbnail from "../../components/ArtistsThumbnail/ArtistsThumbnail";
import {Link} from "react-router-dom";
import {deleteArtist, fetchArtists, publishArtist} from "../../store/actions/artistsAction";

class Artists extends Component {
    componentDidMount() {
        this.props.fetchArtists()
    }

    render() {
        return (
            <Fragment>
                <h1>Artists</h1>

                {this.props.artists ? <CardColumns>
                    {this.props.artists.map(artist => (
                            <Card key={artist._id} >

                                <ProductThumbnail image={artist.image}/>
                                <CardBody>
                                    <CardTitle>
                                    <Link to={'/artists/' + artist._id}>
                                        {artist.name}
                                    </Link>
                                    </CardTitle>
                                    {this.props.user && this.props.user.role === 'admin' && (
                                        <Fragment>
                                            {artist.published === false ? (
                                            <Fragment>
                                                <CardText>
                                                    Неопубликовано
                                                </CardText>
                                                <Button onClick={() => this.props.publishArtist(artist._id)} className="float-right" color='primary'>
                                                    Опубликовать
                                                </Button>
                                            </Fragment>
                                            ): null}
                                            <Button onClick={() => this.props.deleteArtist(artist._id)} color='danger'>
                                                Удалить
                                            </Button>
                                        </Fragment>


                                    )}
                                </CardBody>
                            </Card>

                    ))}
                </CardColumns> : null}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
   artists: state.artists.artists,
   user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchArtists: () => dispatch(fetchArtists()),
    publishArtist: id => dispatch(publishArtist(id)),
    deleteArtist: artistId => dispatch(deleteArtist(artistId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Artists);