import {FETCH_ARTISTS_SUCCESS} from "../actions/artistsAction";

const initialState = {
    artists: null
};

const artistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case  FETCH_ARTISTS_SUCCESS:
            return {
                ...state,
                artists: action.artists,
            };
        default:
            return state
    }
};

export default artistsReducer;