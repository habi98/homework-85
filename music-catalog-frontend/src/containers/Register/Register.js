import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {registerUser} from "../../store/actions/usersActions";
import {connect} from "react-redux";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

class Register extends Component {
    state = {
       username: '',
       password: '',
       displayName: '',
       avatarImage: ''
    };


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.registerUser({...this.state})
    };

    getFieldError = fieldName  => {
        return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message
    };


    render() {
        return (
            <Fragment>
                <h2 className="mt-3 mb-3">Register</h2>
                {this.props.error &&  this.props.error.global &&(
                    <Alert color="danger">
                        {this.props.error.global}
                    </Alert>
                )}
                <Form onSubmit={this.submitFormHandler}>


                    <FormElement
                        title="Login"
                        propertyName="username"
                        type="text"
                        value={this.state.username}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('username')}
                        placeholder="Enter username you registered with"
                        autoComplete="new-username"
                    />

                    <FormElement
                        title="Password"
                        propertyName="password"
                        type="password"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('password')}
                        placeholder="Enter password"
                        autoComplete="new-password"
                    />

                    <FormElement
                        title="Display name"
                        propertyName="displayName"
                        type="text"
                        value={this.state.displayName}
                        onChange={this.inputChangeHandler}
                        placeholder="Enter display name"
                        autoComplete="new-displayName"
                    />

                    <FormElement
                        title="Avatar"
                        propertyName="avatarImage"
                        type="text"
                        value={this.state.avatarImage}
                        onChange={this.inputChangeHandler}
                        placeholder="Enter avatar"
                        autoComplete="new-avatarImage"
                    />

                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="success">
                                Register
                            </Button>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                           <FacebookLogin/>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData))
});


export default connect(mapStateToProps, mapDispatchToProps)(Register);