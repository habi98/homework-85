import axios from "../../axios-music";

import {push} from 'connected-react-router'

export const FETCH_ALBUMS_SUCCESS =  'FETCH_ALBUMS_SUCCESS';
export const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, albums});


export const fetchAlbums = (id) => {
    return dispatch => {

        let url = '/albums';

        if (id) {
            url += `?artist=${id}`
        }
        axios.get(url).then(response => {
            dispatch(fetchAlbumsSuccess(response.data))
        })
    }
};


export const FETCH_ID_ALBUMS_SUCCESS = 'FETCH_ID_ALBUMS_SUCCESS';

export const fetchIdAlbumsSuccess = album => ({type: FETCH_ID_ALBUMS_SUCCESS, album});

export const fetchIdAlbums = id => {
    return dispatch => {
        axios.get('/albums/' + id).then(response => {
            dispatch(fetchIdAlbumsSuccess(response.data))
        })
    }
};


export const CREATE_ALBUMS_SUCCESS = 'CREATE_ALBUMS_SUCCESS';

export const createAlbumsSuccess = () => ({type: CREATE_ALBUMS_SUCCESS});

export const createAlbums = albumsData => {
    return dispatch => {
        axios.post('/albums', albumsData).then(
            dispatch(createAlbumsSuccess()),
            dispatch(push('/'))
        )
    }
};

export const PUBLISH_ALBUMS_SUCCESS = 'PUBLISH_ALBUMS_SUCCESS';
export const publishAlbumsSuccess = () => ({type: PUBLISH_ALBUMS_SUCCESS});


export const publishAlbum = (albumId, artistId) => {
    return dispatch  => {
        axios.post(`/albums/${albumId}/toggle_published`).then(
            () => {
                dispatch(publishAlbumsSuccess());
                dispatch(fetchAlbums(artistId))
            }
        )
    }
};

export const DELETE_ALBUM_SUCCESS = 'DELETE_ALBUM_SUCCESS';

export const deleteAlbumSuccess = () => ({type: DELETE_ALBUM_SUCCESS});

export const deleteAlbum = (albumId, artistsId) => {
    return dispatch => {
        axios.delete('/albums/delete/' + albumId).then(
            () => {dispatch(deleteAlbumSuccess());
                dispatch(fetchAlbums(artistsId))}
        )
    }
};

