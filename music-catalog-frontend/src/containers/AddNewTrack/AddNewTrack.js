import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {fetchAlbums} from "../../store/actions/albumsAction";
import {connect} from "react-redux";
import {createTrack} from "../../store/actions/tracksAction";


class AddNewTrack extends Component {
    state = {
        album: '',
        name: '',
        duration: '',
    };


    componentDidMount() {

        console.log(this.props);
        this.props.fetchAlbums()
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitHandler = event => {
        event.preventDefault();

        this.props.createTrack({...this.state})
    };

    render() {
        return (
            <Form onSubmit={this.submitHandler}>

                <FormElement
                    propertyName="album"
                    title="Album"
                    type="select"
                    onChange={this.inputChangeHandler}
                >

                    <option value="">Please select a Album</option>
                    {this.props.albums && this.props.albums.map(album => (
                        <option value={album._id} key={album._id}>{album.name}</option>
                    ))}
                </FormElement>
                <FormElement
                    propertyName="name"
                    title="Name"
                    type="text"
                    onChange={this.inputChangeHandler}
                />
                <FormElement
                    propertyName="duration"
                    title="Duration"
                    type="text"
                    onChange={this.inputChangeHandler}
                />

                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="primary">Add Track</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}


const mapStateToProps = state => ({
    albums: state.albums.albums
});

const mapDispatchToProps = dispatch => ({
   fetchAlbums: () => dispatch(fetchAlbums()),
    createTrack: data => dispatch(createTrack(data))
});


export default connect(mapStateToProps, mapDispatchToProps)(AddNewTrack);