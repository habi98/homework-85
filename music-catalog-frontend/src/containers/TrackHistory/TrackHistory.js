import React, {Component, Fragment} from 'react';
import {fetchTrackHistory} from "../../store/actions/trackHistoryActions";
import {connect} from "react-redux";
import {Card, CardBody, CardText, CardTitle} from "reactstrap";

class TrackHistory extends Component {
    componentDidMount() {
        this.props.fetchTrackHistory()
    }

    render() {
        return (
            <Fragment>
                <h2 className="pt-4">Track History</h2>
                {this.props.trackHistory && (
                    <Fragment>
                    {this.props.trackHistory.map(history => (

                                <Card className="mt-3" key={history._id}>
                                    <CardBody>
                                        <p>{history.track.album.artist.name}</p>
                                        <CardTitle>{history.track.name}</CardTitle>
                                        <CardText>{history.datetime}</CardText>
                                    </CardBody>
                                </Card>

                        ))}
                    </Fragment>
                )}
            </Fragment>

        );
    }
}


const mapStateToProps = state => ({
    trackHistory: state.trackHistory.trackHistory
});

const mapDispatchToProps = dispatch => ({
    fetchTrackHistory: () => dispatch(fetchTrackHistory())
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);